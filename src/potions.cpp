#include <iostream>
#include <vector>
using namespace std;

class Potion {
public:
  int value;
  Potion(int _value): value(_value){}
  virtual string type () = 0;
  virtual ~Potion() {}
};

class HealingPotion: public Potion {

public:
  using Potion::Potion;
  string type() override {
    return "healing";
  }

  ~HealingPotion() {}

};

class ManaPotion: public Potion {
public:
  using Potion::Potion;
  string type() override {
    return "mana";
  }
  ~ManaPotion() {}
};

class RestorePotion: public Potion {
public:
  using Potion::Potion;
  string type() override {
    return "restore";
  }
  ~RestorePotion() {}

};

class PotionBackpack {
private:
  vector<Potion*> backpack;

public:
  void add_potion(Potion *p) {
    backpack.push_back(p);
  }

  void print_potion_types() {
    for(auto p: backpack) {
      cout << p->type() << "\n";
    }
  }

  auto get_potion() {
    auto val = backpack.front();
    backpack.erase(backpack.begin());
    return val;
  }

  ~PotionBackpack() {
    for(auto p: backpack)
      delete p;
  }

};

class Character {
private:
  int health, mana;
public:
  Character(int _health, int _mana): health(_health), mana(_mana) {}

  void status() {
    cout << "health " << health << "\nmana " << mana << endl;
  }

  void drink_potion(auto x) {
    if(x->type() == "mana") {
      this->mana += x->value;
    }
    else if(x->type() == "healing") {
      this->health += x->value;
    }

    else if(x->type() == "restore") {
      this->health += x->value;
      this->mana += x->value;
    }
  }
  ~Character() {}
};

int main() {
  PotionBackpack b;
  b.add_potion(new ManaPotion(10));
  b.add_potion(new HealingPotion(20));
  b.add_potion(new RestorePotion(10));
  b.print_potion_types();
  cout<<endl;
  Character c(50,20);
  c.status();
  c.drink_potion(b.get_potion());
  c.status();
  cout<<endl;
  b.print_potion_types();
  cout<<endl;
  c.drink_potion(b.get_potion());
  c.status();
  cout<<endl;
  b.print_potion_types();
  cout<<endl;
  c.drink_potion(b.get_potion());
  c.status();
  cout<<endl;
  b.print_potion_types();
}
